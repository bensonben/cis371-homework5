<?php
$get = $_GET["state"];
$servername = "cis.gvsu.edu";
$username = "bensonb";
$password = "bensonb1069";
$dbname = "bensonb";


$DOMdoc = new DomDocument("1.0", "ISO-8850-1");
header("content-type: text/xml");
//create the root of the XML document named customers.
$root = $DOMdoc->createElement("Customers");
//create an attribute in the dom document called state
$rootAttribute = $DOMdoc->createAttribute("State");
//append the root attribute from the GET ReST service.
$rootAttribute->value = $get;


$root->appendChild($rootAttribute);
$DOMdoc->appendChild($root);

$conn = mysqli_connect($servername, $username, $password, $dbname);
if (!$conn) {
    die("Connection failed: " . $conn->mysqli_connect_error());
}

// id INT(6) UNSIGNED PRIMARY KEY,
// 	nameFirst VARCHAR(30) NOT NULL,
// 	nameLast VARCHAR(30) NOT NULL,
// 	address VARCHAR(100) NOT NULL,
// 	city VARCHAR(30) NOT NULL,
// 	st VARCHAR(30) NOT NULL,
// 	zip VARCHAR(12) NOT NULL,
// 	creditCard VARCHAR(12) NOT NULL,
// 	balance DOUBLE(100,2) NOT NULL
 $sql = "select * from customers where st = '".strtoupper($get)."' ";

 $result_set = $conn->query($sql);

 if(!$result_set.num_rows){
     while($row = $result_set->fetch_assoc()){
        //add a customer element to the document, and a customer has attributes.
        $customer = $DOMdoc->createElement("Customer");
        $attribute = $DOMdoc->createElement("ID", $row['id']);
        $customer->appendChild($attribute);

        $attribute = $DOMdoc->createElement("firstName", $row['nameFirst']);
        $customer->appendChild($attribute);

        $attribute= $DOMdoc->createElement("LastName", $row['nameLast']);
        $customer->appendChild($attribute);

        $attribute= $DOMdoc->createElement("City", $row['city']);
        $customer->appendChild($attribute);

        $attribute = $DOMdoc->createElement("Address", $row['address']);
        $customer->appendChild($attribute);

        $attribute = $DOMdoc->createElement("Street", $row['st']);
        $customer->appendChild($attribute);

        $attribute = $DOMdoc->createElement("ZipCode", $row['zip']);
        $customer->appendChild($attribute);

        $attribute = $DOMdoc->createElement("CreditCard", $row['creditCard']);
        $customer->appendChild($attribute);

        $attribute = $DOMdoc->createElement("Balance", $row['balance']);
        $customer->appendChild($attribute);

        $root->appendChild($customer);
    }
}

// $conn->close();
echo $DOMdoc->saveHTML();
?>