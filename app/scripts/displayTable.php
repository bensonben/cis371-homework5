<!doctype html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <title>cis371 hw5</title>
   
  </head>
  <body>
    
    <div class="container">
      <div class="header">        
        <h3 class="text-muted">CIS 371 Homework 5: Customer List</h3>
      </div>

      <div class="jumbotron">
		<?php
			$servername = "cis.gvsu.edu";
			$username = "bensonb";
			$password = "bensonb1069";
			$dbname = "bensonb";

			// Create connection
			$conn = mysqli_connect($servername, $username, $password, $dbname);

			// Check connection
			if (!$conn) {
			    die("Connection failed: " . mysqli_connect_error());
			}
			// echo "Connected successfully";

			$sql = "SELECT * FROM customers"; 
			$result = $conn->query($sql);

			echo "<div>";
			echo "<table class='table table-striped'>";
			echo "<caption>Customers Table</caption>";
			echo "<thead>";
			echo "<tr>";
			echo "<th>ID#</th>" ;
			echo "<th>First Name</th>";
			echo "<th>Last Name</th>";
			echo "<th>Address</th>"; 
			echo "<th>City</th>"; 
			echo "<th>State</th>"; 
			echo "<th>ZIP</th>"; 
			echo "<th>CreditCard</th>"; 
			echo "<th>balance</th>"; 
			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			if (!$result_set.num_rows) {    
			    while($row = $result->fetch_assoc()) {
			    echo "<tr>";
			    echo "<th>" .$row['id']."</th>"; 
			    echo "<td>" .$row['nameFirst']."</td>"; 
			    echo "<td>" .$row['nameLast']."</td>";
				echo "<td>" .$row['address']."</td>";
				echo "<td>" .$row['city']."</td>";
				echo "<td>" .$row['st']."</td>";
				echo "<td>" .$row['zip']."</td>";
				echo "<td>" .$row['creditCard']."</td>";
				echo "<td>" .$row['balance']."</td>"; 
			        echo "</tr>";
				}
				echo "</tbody>";
				echo "</table>";
				echo "</div>";
			} else {
			    echo "0 results";
			}
			$conn->close();
		?>
	  </div>
    </div>
    
  </body>
</html>
