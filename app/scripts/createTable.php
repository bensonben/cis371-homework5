<?php
	$servername = "cis.gvsu.edu";
	$username = "bensonb";
	$password = "bensonb1069";
	$dbname = "bensonb";
	//id,nameFirst,nameLast,address,city,st,zip,creditCard,balance

	// Create connection
	$conn = mysqli_connect($servername, $username, $password, $dbname);

	// Check connection
	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}
	echo "Connected successfully";

	// sql to create table
	$sql = "CREATE TABLE customers(
		id INT(6) UNSIGNED PRIMARY KEY, 
		nameFirst VARCHAR(30) NOT NULL,
		nameLast VARCHAR(30) NOT NULL,
		address VARCHAR(100) NOT NULL,
		city VARCHAR(30) NOT NULL,
		st VARCHAR(30) NOT NULL,
		zip VARCHAR(12) NOT NULL,
		creditCard VARCHAR(12) NOT NULL,
		balance DOUBLE(100,2) NOT NULL
	)";

	if ($conn->query($sql) === TRUE) {
	    echo "Table customers created successfully <br>";
	} else {
	    echo "Error creating table: " . $conn->error;
	}
	$conn->close();
?>
